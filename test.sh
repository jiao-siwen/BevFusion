#!/bin/bash
module purge
module load anaconda cuda/11.1 complier/gcc/12.2.0 openmpi/4.1.1
#module load anaconda cuda/11.1 gcc/5.4 openmpi/4.1.1
date
# tar -xf /HOME/scz0o1i/.conda/envs/bevfusion.tar.gz -C /dev/shm
# date
# source activate /dev/shm/bevfusion
source activate bevfusion
export PYTHONUNBUFFERED=1
export PYTHONPATH=`pwd`:$PYTHONPATH
export CUDA_HOME='/data/apps/cuda/11.1'
#torchpack dist-run -np 1 python tools/train.py --config_s configs/nuscenes/det/centerhead/lssfpn/camera/256x704/swint/default.yaml   --config_t configs/nuscenes/det/transfusion/secfpn/lidar/voxelnet_0p075.yaml   --checkpoint_t pretrained/lidar-only-det.pth  --model.encoders.camera.backbone.init_cfg.checkpoint pretrained/swint-nuimages-pretrained.pth
#python tools/create_data.py nuscenes --root-path ./data/nuscenes --out-dir ./data/nuscenes --extra-tag nuscenes
#torchpack dist-run -np 1 python tools/train.py --config_s configs/nuscenes/det/centerhead/lssfpn/camera/256x704/swint/default.yaml   --config_t configs/nuscenes/det/transfusion/secfpn/camera+lidar/swint_v0p075/convfuser.yaml   --checkpoint_t pretrained/bevfusion-det.pth  --model.encoders.camera.backbone.init_cfg.checkpoint pretrained/swint-nuimages-pretrained.pth
#python setup.py develop
torchpack dist-run -np 1 python tools/test.py configs/nuscenes/det/transfusion/secfpn/lidar/voxelnet_0p075.yaml runs/run-e71c55c3/epoch_20.pth --eval bbox
