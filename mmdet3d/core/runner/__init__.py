'''
Author: Peng Bo
Date: 2022-10-14 17:02:11
LastEditTime: 2022-10-14 17:15:45
Description: 

'''
# Copyright (c) OpenMMLab. All rights reserved.
from .distillation_runner import DistillationRunner
from .custom_distillation_runner import CustomDistillationRunner

__all__ = ['DistillationRunner','CustomDistillationRunner']